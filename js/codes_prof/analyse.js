
/**********ANALYSE SON**********/

var audio = document.querySelector("audio");
var audioCtx = new AudioContext();
var analyser = audioCtx.createAnalyser();
var source = audioCtx.createMediaElementSource(audio);
source.connect(analyser);
analyser.connect(audioCtx.destination);
audio.play();
var frequencyData = new Uint8Array(analyser.frequencyBinCount);
function analyseSound() {
    analyser.getByteFrequencyData(frequencyData);
    console.log(frequencyData);
    window.requestAnimationFrame(analyseSound);
}
window.requestAnimationFrame(analyseSound);