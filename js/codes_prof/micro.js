
/**********RECUP MICRO 1**********/

var sound = [];
var bufferSize = 16384;

//http://www.softfluent.fr/blog/expertise/2015/04/28/Enregistrer-du-son-via-le-Microphone-en-JavaScript


// document.querySelector("body").onload = function(){
    micro = function() {
    sound = document.querySelector("#sound");
    c =  sound.getContext("2d");
    var w = sound.width;
    var h = sound.height;

    navigator.mediaDevices.getUserMedia({ audio: true, video: false })
        .then(function(stream) {
            window.AudioContext = window.AudioContext || window.webkitAudioContext;
            context = new AudioContext();
            mediaStream = context.createMediaStreamSource(stream);
            var numberOfInputChannels = 2;
            var numberOfOutputChannels = 2;
            if (context.createScriptProcessor) {
                recorder = context.createScriptProcessor(bufferSize, numberOfInputChannels, numberOfOutputChannels);
            } else {
                recorder = context.createJavaScriptNode(bufferSize, numberOfInputChannels, numberOfOutputChannels);
            }

            recorder.onaudioprocess = function (e) {
                var chanl = e.inputBuffer.getChannelData(0);
                var chanr = e.inputBuffer.getChannelData(1);
                sound = chanr;
            };
            mediaStream.connect(recorder);
            recorder.connect(context.destination);

        })
        .catch(function(err) {
            console.log("Stream not OK");
        });

    setInterval(function() {
        c.clearRect(0,0,w,h);
        c.beginPath();
        c.moveTo(0,h/2);
        for (i in sound) {
            c.lineTo((i*w)/bufferSize,h/2+sound[i]*300);
        }
        c.stroke();
    }, 1);

};

document.querySelector("body").onload = micro();


/**********RECUP MICRO 2**********/

var sound = [];
var bufferSize = 256;
var amplitude = 0;
var maxAmplitude = 0;
var frequence = 0;

//http://www.softfluent.fr/blog/expertise/2015/04/28/Enregistrer-du-son-via-le-Microphone-en-JavaScript

// document.querySelector("body").onload = function(){
micro = function() {
    sound = document.querySelector("#sound");
    c =  sound.getContext("2d");
    var w = sound.width;
    var h = sound.height;

    navigator.mediaDevices.getUserMedia({ audio: true, video: false })
        .then(function(stream) {
            window.AudioContext = window.AudioContext || window.webkitAudioContext;
            context = new AudioContext();
            mediaStream = context.createMediaStreamSource(stream);
            var numberOfInputChannels = 2;
            var numberOfOutputChannels = 2;
            if (context.createScriptProcessor) {
                recorder = context.createScriptProcessor(bufferSize, numberOfInputChannels, numberOfOutputChannels);
            } else {
                recorder = context.createJavaScriptNode(bufferSize, numberOfInputChannels, numberOfOutputChannels);
            }

            recorder.onaudioprocess = function (e) {
                var chanl = e.inputBuffer.getChannelData(0);
                var chanr = e.inputBuffer.getChannelData(1);
                amplitude = 0;
                frequence = 0;
                var mem = 0;
                var l = 0;
                var r = 0;
                for (i in chanl) {
                    l = chanl[i];
                    r = chanr[i];
                    amplitude += Math.abs(l)+Math.abs(r);
                    if ((l<0 && mem>0) || (l>0 && mem<0))
                        frequence++;
                    mem = l;
                }
                if (maxAmplitude < amplitude){
                    maxAmplitude = amplitude;
                }
            };
            mediaStream.connect(recorder);
            recorder.connect(context.destination);

        }).catch(function(err) {
        console.log("Stream not OK");

    });

    setInterval(function() {
        c.clearRect(0,0,w,h);
        c.beginPath();
        c.moveTo(0,h/2);
        for (i in sound) {
            c.lineTo((i*w)/bufferSize,h/2+sound[i]*300);
        }
        c.stroke();
    }, 1);

};

document.querySelector("body").onload = micro();