var sound = [];
var bufferSize = 256;
var amplitude = 0;
var maxAmplitude = 0;
var frequence = 0;

var particuleAlphabet = {
    Particule: function(x, y) {
        this.x = x;
        this.y = y;
        this.radius = 2;
        this.draw = function(ctx) {
            ctx.save();
            ctx.beginPath();
            ctx.translate(this.x, this.y);
            ctx.fillStyle = 'rgb(134, 6, 49)';
            ctx.arc(0, 0, this.radius, 0, Math.PI*2, true);
            ctx.fill();
            ctx.restore();
        };
    },
    init: function() {
        particuleAlphabet.canvas = document.querySelector('#alphabet');
        particuleAlphabet.ctx = particuleAlphabet.canvas.getContext('2d');
        particuleAlphabet.W = window.innerWidth;
        particuleAlphabet.H = window.innerHeight;
        particuleAlphabet.particulePositions = [];
        particuleAlphabet.particules = [];
        particuleAlphabet.tmpCanvas = document.createElement('canvas');
        particuleAlphabet.tmpCtx = particuleAlphabet.tmpCanvas.getContext('2d');

        particuleAlphabet.canvas.width = particuleAlphabet.W;
        particuleAlphabet.canvas.height = particuleAlphabet.H;

        particuleAlphabet.changeLetter();
        particuleAlphabet.getPixels(particuleAlphabet.tmpCanvas, particuleAlphabet.tmpCtx);

        // setInterval(function(){
        //     particuleAlphabet.changeLetter();
        //     particuleAlphabet.getPixels(particuleAlphabet.tmpCanvas, particuleAlphabet.tmpCtx);
        // }, 1200);

        particuleAlphabet.makeParticules(10000);
        particuleAlphabet.animate();
    },
    currentPos: 0,
    changeLetter: function() {
        var letters = 'ABCDEFGHIJKLMNOPQRSTUVXYZ',
            letters = 'Hello world !';
            letters = letters.split(' ');
        particuleAlphabet.time = letters[particuleAlphabet.currentPos];
        particuleAlphabet.currentPos++;
        if (particuleAlphabet.currentPos >= letters.length) {
            particuleAlphabet.currentPos = 0;
        }
    },
    makeParticules: function(num) {
        for (var i = 0; i <= num; i++) {
            particuleAlphabet.particules.push(new particuleAlphabet.Particule(particuleAlphabet.W / 2 + Math.random() * 400 - 200, particuleAlphabet.H / 2 + Math.random() * 400 - 200));
        }
    },
    getPixels: function(canvas, ctx) {
        var keyword = particuleAlphabet.time,
            gridX = 5,
            gridY = 5;
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
        fontSize = Math.round(amplitude)*3;
        if (frequence > 20){
            ctx.font = fontSize*3+'px Noto serif';
        } else if (frequence <= 20 && frequence > 10){
            ctx.font = fontSize*3+'px Arial';
        } else {
            ctx.font = fontSize*3+'px Indie Flower';
        }
        ctx.fillText(keyword, canvas.width / 2 - ctx.measureText(keyword).width / 2, canvas.height / 2 + 100);
        var idata = ctx.getImageData(0, 0, canvas.width, canvas.height);
        var buffer32 = new Uint32Array(idata.data.buffer);
        if (particuleAlphabet.particulePositions.length > 0) particuleAlphabet.particulePositions = [];
        for (var y = 0; y < canvas.height; y += gridY) {
            for (var x = 0; x < canvas.width; x += gridX) {
                if (buffer32[y * canvas.width + x]) {
                    particuleAlphabet.particulePositions.push({x: x, y: y});
                }
            }
        }
    },
    animateParticules: function() {
        var p, pPos;
        for (var i = 0, num = particuleAlphabet.particules.length; i < num; i++) {
            p = particuleAlphabet.particules[i];
            pPos = particuleAlphabet.particulePositions[i];
            console.log(p);
            cnosole.log(pPos);
            if (particuleAlphabet.particules.indexOf(p) === particuleAlphabet.particulePositions.indexOf(pPos)) {
                p.x += (pPos.x - p.x) * 0.3;
                p.y += (pPos.y - p.y) * 0.3;
                p.draw(particuleAlphabet.ctx);
            }
        }
    },
    animate: function() {
        requestAnimationFrame(particuleAlphabet.animate);
        particuleAlphabet.ctx.fillStyle = 'bisque';
        particuleAlphabet.ctx.fillRect(0, 0, particuleAlphabet.W, particuleAlphabet.H);
        particuleAlphabet.animateParticules();
    },
    micro: function(){
        sound = document.querySelector("#sound");
        c =  sound.getContext("2d");
        var w = sound.width;
        var h = sound.height;

        navigator.mediaDevices.getUserMedia({ audio: true, video: false })
            .then(function(stream) {
                window.AudioContext = window.AudioContext || window.webkitAudioContext;
                context = new AudioContext();
                mediaStream = context.createMediaStreamSource(stream);
                var numberOfInputChannels = 2;
                var numberOfOutputChannels = 2;
                if (context.createScriptProcessor) {
                    recorder = context.createScriptProcessor(bufferSize, numberOfInputChannels, numberOfOutputChannels);
                } else {
                    recorder = context.createJavaScriptNode(bufferSize, numberOfInputChannels, numberOfOutputChannels);
                }

                recorder.onaudioprocess = function (e) {
                    var chanl = e.inputBuffer.getChannelData(0);
                    var chanr = e.inputBuffer.getChannelData(1);
                    // sound = chanr;
                    amplitude = 0;
                    frequence = 0;
                    var mem = 0;
                    var l = 0;
                    var r = 0;
                    for (i in chanl) {
                        l = chanl[i];
                        r = chanr[i];
                        amplitude += Math.abs(l)+Math.abs(r);
                        if ((l<0 && mem>0) || (l>0 && mem<0))
                            frequence++;
                        mem = l;
                    }
                    if (maxAmplitude<amplitude)
                        maxAmplitude = amplitude;
                };
                mediaStream.connect(recorder);
                recorder.connect(context.destination);

            }).catch(function(err) {
            console.log("Stream not OK");

        });

        setInterval(function() {
            c.clearRect(0,0,w,h);
            c.beginPath();
            c.moveTo(0,h/2);
            for (i in sound) {
                c.lineTo((i*w)/bufferSize,h/2+sound[i]*300);
            }
            c.stroke();
            if (amplitude > 20){
                particuleAlphabet.init();
                console.log(document.querySelector('#valeurs'));
                var div = document.querySelector('#valeurs');
                if (div.querySelector("p")){
                    div.querySelector(("p")).remove();
                }
                amplitude = Math.round(amplitude);
                var para = document.createElement("p");
                var text = document.createTextNode('Fréquence : '+frequence+', Amplitude : '+amplitude);
                para.appendChild(text);
                div.appendChild(para);
            }
        }, 1);
    }
};

var frequenceMicro = {

};

// window.onload = frequenceMicro.micro;
window.onload = particuleAlphabet.micro;
// window.onmousedown = particuleAlphabet.init;