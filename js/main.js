/**********COURS**********/
var alphabet, c, w, h, a, mouse;

document.querySelector("body").onload = function(){
    canvas = document.querySelector("canvas");
    c =  canvas.getContext("2d");
    w = canvas.width;
    h = canvas.height;
    mouse = {x: 0, y: 0};
    alphabet.onmousemove = function(event){
        mouse = { x: event.clientX, y: event.clientY};
    };
    a = 0;
    // draw();
    setInterval(draw, 30);
};

function draw(){

/*TRAITS*/
    // c.beginPath();
    // c.strokeStyle = "orange";
    // c.moveTo(1, 20);
    // c.lineTo(100, 80);
    // c.moveTo(200, 140);
    // c.lineTo(300, 180);
    // c.stroke();

/*SMILEY*/
    c.clearRect(0,0,w,h);
    c.save();
    c.beginPath();
    c.translate(mouse.x, mouse.y);
    // c.translate(200, 200);
    c.rotate(a);
    a += Math.PI/360;

    c.beginPath();
    c.fillStyle = "orange";
    c.arc(0, 0, 100, 0, Math.PI*2, true);
    c.fill();

    c.beginPath();
    c.fillStyle = "black";
    c.arc(-30, -30, 10, 0, Math.PI*2, true);
    c.fill();

    c.beginPath();
    c.fillStyle = "black";
    c.arc(30, -30, 20, 0, Math.PI*2, true);
    c.fill();

    c.beginPath();
    c.fillStyle = "black";
    c.arc(0, 30, 50, 0, Math.PI, false);
    c.fill();

    c.restore();
}